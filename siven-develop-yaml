---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: siven-disk-develop
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: siven-deployment-develop
  labels:
    app: siven-develop
spec:
  replicas: 1
  strategy:
    rollingUpdate:
      maxSurge: 50%
      maxUnavailable: 30%
    type: RollingUpdate
  selector:
    matchLabels:
      app: siven-develop
  template:
    metadata:
      labels:
        app: siven-develop
    spec:
      containers:
        - name: siven-container-develop
          image: sieven/siven:develop
          imagePullPolicy: Always
          ports:
            - containerPort: 80
          volumeMounts:
            - mountPath: "/var/www/html/siven"
              subPath: "siven"
              name: siven-disk-develop  
          env:
            - name: bd-mysql-latest
              value: mysql-service-latest
            - name: siven-bd-pass
              valueFrom:
                secretKeyRef:
                  name: mysql-secrets
                  key: ROOT_PASSWORD
      volumes:
        - name: siven-disk-develop
          persistentVolumeClaim:
            claimName: siven-disk-develop       
---
apiVersion: v1
kind: Service
metadata:
  name: siven-develop
spec:
  type: NodePort  
  selector:
    app: siven-develop
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
